

puts "Enter the fixed number to multiply each stage by:\n" # ask for the fixed number
x = gets.chomp # read input into x
y = 0 # set y
i = 1 # set i

while i == 1 # change the value of i to break out
  if y == 0 # if this is the first round
    puts "Enter the square of the fixed number:\n" # ask for the square
    y = gets.chomp # read the input into y
    z = x.to_i * x.to_i # set z to the correct answer
  else
    puts "Enter the last number you entered multiplied by the fixed number:\n" # ask for the next number in the series
    z = y.to_i * x.to_i # set z to the correct answer
    y = gets.chomp # read input into y
  end
  if y.to_i != z.to_i # if the answer given was incorrect
    puts "Sorry, the answer was " + z.to_s + ", you lose. Please play again." # tell the player they lost
    i = 0 # break out of the while loop
  end
end
