

print "Fahrenheit or Celsius? \n"
a = gets.chomp
b = 1

while b == 1
  if a == 'fahrenheit'
    print "Enter your temperature in fahrenheit \n"
    x = gets.chomp
    c = ((x.to_i - 32) * 5/9)
    print 'Your temperature in celsius is ' + c.to_s + "\n"
    print "Fahrenheit or Celsius? \n"
    a = gets.chomp
  elsif a == 'celsius'
    print "Enter your temperature in celsius \n"
    x = gets.chomp
    f = (x.to_i * 9/5) + 32
    print 'Your temperature in fahrenheit is ' + f.to_s + "\n"
    print "Fahrenheit or Celsius? \n"
    a = gets.chomp
  elsif a == 'quit'
    b = 1
    break
  else
    print "Fahrenheit or Celsius? \n"
    a = gets.chomp
  end
end
